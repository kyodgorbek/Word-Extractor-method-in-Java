# Word-Extractor-method-in-Java



import javax.swing.JOptionPane;

public class WordExtractorApp {
 
  public static void main(String[] args){
   // Read in a sentence
   
   String sent = JOptionPane.showInputDialog("Enter at least 4 words");
   
   // Create a WordExtractor object that stores the input sentence
   WordExtractor wE1 = new WordExtractor(sent);
   
   //Write the first word to the console window.
  System.out.println("first word: "  + wE1.getFirst());
  
  // Create a WordExtractor object that stores the sentence
  // starting with second word
  WordExtractor wE2 = new WordExtractor(wE1.getRest());
  // Write the second word to the console window
  System.out.println("second word: " + wE2.getFirst());
  
  //Create a WordExtractor object that stores the sentence
  // starting with the third word
  WordExtractor wE3  = new WordExtractor(wE2.getRest());
  // Write the third word to the console window
  System.out.println("third word: " + wE3.getFirst());
  
  // Write the rest of the sentence to the console window
  System.out.println("rest of sentence: " + wE3.getRest);
 }
}   
